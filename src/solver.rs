use std::collections::{HashMap, HashSet, VecDeque};

use bumpalo::Bump;
use log::*;

use crate::{Literal, WorkVec};

use bcp::BCPResult;
use clause::{Clause, ClauseState};

mod bcp;
mod clause;
mod conflict_analysis;

type DecisionLevel = usize;
type ClauseIndex = usize;

pub struct Solver<'bump> {
    arena: &'bump Bump,

    decision_level: DecisionLevel,
    original_clause_count: usize,
    clauses: Vec<Clause<'bump>>,
    assignment_stack: AssignmentStack,
    antecedent: HashMap<Literal, Antecedent>,
    score: HashMap<usize, usize>,
    watched_literals: WatchedLiterals,
    propagation_queue: VecDeque<(Literal, Antecedent)>,

    conflict_clause: WorkVec<Literal>,
    pivots: WorkVec<Literal>,
}

impl<'bump> Solver<'bump> {
    pub fn new(arena: &'bump Bump) -> Self {
        Self {
            arena,

            decision_level: 0,
            original_clause_count: 0,
            clauses: Vec::new(),
            assignment_stack: AssignmentStack::new(),
            antecedent: HashMap::new(),
            score: HashMap::new(),
            watched_literals: WatchedLiterals::new(),
            propagation_queue: VecDeque::new(),

            conflict_clause: WorkVec::new(Vec::new()),
            pivots: WorkVec::new(Vec::new()),
        }
    }

    pub fn var_count(&self) -> usize {
        let mut var_set = HashSet::new();
        self.clauses
            .iter()
            .flat_map(|clause| clause.iter())
            .for_each(|lit| {
                var_set.insert(lit.var());
            });

        var_set.len()
    }

    pub fn clause_count(&self) -> usize {
        self.clauses.len()
    }

    pub fn assert(&mut self, clause: impl Iterator<Item = Literal>) {
        let clause = Clause::from_iter_in(clause, self.arena);

        for &literal in clause.iter() {
            *self.score.entry(literal.var()).or_insert(0) += 1;
        }

        match clause.len() {
            0 => (),

            1 => self
                .propagation_queue
                .push_back((clause[0], Antecedent::OneLiteralClause)),

            _ => {
                let clause_index = self.clauses.len();

                self.watched_literals.select_watched_literals(
                    clause_index,
                    &clause,
                    &self.assignment_stack,
                );

                self.clauses.push(clause);
            }
        }
    }

    pub fn assert_exactly_one(&mut self, literals: &[Literal]) {
        self.assert(literals.iter().copied());

        for (index, value_a) in literals.iter().copied().enumerate() {
            for value_b in literals[index + 1..].iter().copied() {
                self.assert([-value_a, -value_b].into_iter());
            }
        }
    }

    fn learned_clauses(&self) -> &[Clause<'bump>] {
        &self.clauses[self.original_clause_count..]
    }

    pub fn solve(&mut self) -> SolveResult {
        for (literal, _) in self.propagation_queue.iter() {
            if self
                .propagation_queue
                .iter()
                .any(|(lit, _)| *literal == -*lit)
            {
                return SolveResult::Unsat;
            }
        }

        self.original_clause_count = self.clauses.len();

        loop {
            match self.bcp() {
                BCPResult::Sat => return SolveResult::Sat,
                BCPResult::Unresolved => self.decide(),

                BCPResult::Conflict(conflict_clause) => {
                    if self.decision_level == 0 {
                        return SolveResult::Unsat;
                    }

                    let conflict_result = self.analyze_conflict(conflict_clause);
                    self.backtrack(conflict_result.backtrack_level());

                    match conflict_result {
                        conflict_analysis::ConflictAnalysisResult::NewClause {
                            asserted_literal,
                            clause_index,
                            ..
                        } => {
                            let clause = &self.clauses[clause_index];

                            debug_assert_eq!(
                                self.clauses.last().unwrap().state(&self.assignment_stack),
                                ClauseState::Unit(asserted_literal)
                            );

                            self.watched_literals.select_watched_literals(
                                clause_index,
                                clause,
                                &self.assignment_stack,
                            );
                        }

                        conflict_analysis::ConflictAnalysisResult::SingleLiteral(_) => {
                            debug_assert_eq!(self.decision_level, 0)
                        }
                    }

                    let asserted_literal = conflict_result.asserted_literal();
                    self.propagation_queue.push_back((
                        asserted_literal,
                        conflict_result.asserted_literal_antecedent(),
                    ));
                }
            }
        }
    }

    fn decide(&mut self) {
        debug_assert!({
            match self
                .clauses
                .iter()
                .find(|clause| matches!(clause.state(&self.assignment_stack), ClauseState::Unit(_)))
            {
                Some(clause) => {
                    eprintln!("at least one unit clause left after bcp: {clause:?}");
                    false
                }
                None => true,
            }
        });

        // Berkmin decision heuristic
        let literal = self
            .learned_clauses()
            .iter()
            .rev()
            .find_map(|clause| {
                clause
                    .iter()
                    .copied()
                    .filter(|&lit| self.assignment_stack.check_assignment(lit).is_none())
                    .max_by(|lit_a, lit_b| self.score[&lit_a.var()].cmp(&self.score[&lit_b.var()]))
            })
            .or_else(|| {
                self.clauses
                    .iter()
                    .flat_map(|clause| clause.iter())
                    .copied()
                    .filter(|&lit| self.assignment_stack.check_assignment(lit).is_none())
                    .max_by(|lit_a, lit_b| self.score[&lit_a.var()].cmp(&self.score[&lit_b.var()]))
            })
            .expect("There must be at least one unassigned literal when deciding");

        self.decision_level += 1;
        trace!(target: "literals", "deciding {literal:?}@{}", self.decision_level);
        debug_assert!(self.propagation_queue.is_empty());
        self.propagation_queue
            .push_back((literal, Antecedent::Decision));
    }

    fn backtrack(&mut self, backtrack_level: usize) {
        trace!("backtracking to {backtrack_level}");
        while let Some(literal) = self.assignment_stack.pop_until(backtrack_level) {
            self.antecedent.remove(&literal);
        }
        self.decision_level = backtrack_level;
    }

    pub fn model(&self) -> Vec<Literal> {
        self.assignment_stack.iter().map(|entry| entry.0).collect()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum SolveResult {
    Sat,
    Unsat,
}

#[derive(Clone)]
pub(crate) struct AssignmentStack {
    stack: Vec<AssignmentEntry>,
    index: HashMap<usize, usize>,
}

impl std::ops::Deref for AssignmentStack {
    type Target = Vec<AssignmentEntry>;

    fn deref(&self) -> &Self::Target {
        &self.stack
    }
}

impl std::ops::DerefMut for AssignmentStack {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.stack
    }
}

impl std::fmt::Debug for AssignmentStack {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_list().entries(self.stack.iter()).finish()
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) struct AssignmentEntry(Literal, DecisionLevel);

impl AssignmentEntry {
    pub fn lit(self) -> Literal {
        self.0
    }
}

impl std::fmt::Debug for AssignmentEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "assert {:?}@{}", self.0, self.1)
    }
}

impl AssignmentStack {
    pub fn new() -> Self {
        Self {
            stack: Vec::new(),
            index: HashMap::new(),
        }
    }

    pub fn assign(&mut self, literal: Literal, dl: DecisionLevel) {
        self.index.insert(literal.var(), self.stack.len());
        self.stack.push(AssignmentEntry(literal, dl));
    }

    pub fn pop_until(&mut self, min_decision_level: DecisionLevel) -> Option<Literal> {
        if self.stack.last()?.1 <= min_decision_level {
            return None;
        }

        self.stack.pop().map(AssignmentEntry::lit).inspect(|lit| {
            self.index.remove(&lit.var());
        })
    }

    /// Returns the literal in `literals` whose assignment is shallowest in the assignment stack.
    pub fn shallowest(&self, literals: &[Literal]) -> Option<Literal> {
        self.stack
            .iter()
            .rev()
            .copied()
            .map(|entry| entry.0)
            .find(|lit| literals.iter().any(|other| other.var() == lit.var()))
    }

    pub fn check_assignment(&self, literal: Literal) -> Option<(bool, DecisionLevel)> {
        self.index
            .get(&literal.var())
            .map(|&index| self.stack[index])
            .map(|AssignmentEntry(lit, dl)| (lit.polarity() == literal.polarity(), dl))
    }
}

impl Default for AssignmentStack {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Antecedent {
    Decision,
    Implication(usize),
    OneLiteralClause,
}

#[derive(Debug, Clone)]
struct WatchedLiterals {
    store: HashMap<Literal, Vec<ClauseIndex>>,
}

impl std::ops::Deref for WatchedLiterals {
    type Target = HashMap<Literal, Vec<ClauseIndex>>;

    fn deref(&self) -> &Self::Target {
        &self.store
    }
}

impl std::ops::DerefMut for WatchedLiterals {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.store
    }
}

impl WatchedLiterals {
    pub fn new() -> Self {
        Self {
            store: HashMap::new(),
        }
    }

    pub fn move_watched_literal(&mut self, clause_index: ClauseIndex, from: Literal, to: Literal) {
        trace!(target: "wl", "moving watched literal from {from:?} to {to:?}");

        let watchlist = self.store.entry(from).or_default();
        let old_pos = watchlist
            .iter()
            .copied()
            .position(|index| index == clause_index)
            .unwrap();
        watchlist.swap_remove(old_pos);

        self.store.entry(to).or_default().push(clause_index);
    }

    pub fn select_watched_literals(
        &mut self,
        clause_index: ClauseIndex,
        clause: &Clause,
        assignment_stack: &AssignmentStack,
    ) {
        let first = clause
            .select_watched_literal(assignment_stack, None)
            .unwrap();
        let second = clause
            .select_watched_literal(assignment_stack, Some(first))
            .ok_or_else(|| {
                error!(
                    "Failed to select a second watched literal in {}",
                    clause.display(assignment_stack)
                );
            })
            .unwrap();

        trace!("watching {first:?} and {second:?} in {clause:?}");

        self.entry(first).or_default().push(clause_index);
        self.entry(second).or_default().push(clause_index);
    }
}
