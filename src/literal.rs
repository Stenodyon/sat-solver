use colored::Colorize;

use crate::AssignmentStack;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Polarity {
    Positive,
    Negative,
}

impl Polarity {
    pub fn opposite(self) -> Self {
        match self {
            Self::Positive => Self::Negative,
            Self::Negative => Self::Positive,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Literal(isize);

impl Literal {
    pub fn var(self) -> usize {
        self.0.unsigned_abs() - 1
    }

    pub fn polarity(&self) -> Polarity {
        if self.0 >= 0 {
            Polarity::Positive
        } else {
            Polarity::Negative
        }
    }

    pub fn pos(var: usize) -> Self {
        Self(var as isize + 1)
    }

    pub fn neg(var: usize) -> Self {
        Self(-(var as isize + 1))
    }

    pub fn is_pos(&self) -> bool {
        self.0 >= 0
    }

    pub fn is_neg(&self) -> bool {
        self.0 < 0
    }
}

impl std::ops::Neg for Literal {
    type Output = Literal;

    fn neg(self) -> Self::Output {
        Literal(-self.0)
    }
}

impl std::fmt::Debug for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.is_neg() {
            write!(f, "¬")?;
        }
        write!(f, "{}", self.var())
    }
}

impl std::fmt::Display for Literal {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.is_neg() {
            write!(f, "-")?;
        }
        write!(f, "{}", self.var())
    }
}

impl From<isize> for Literal {
    fn from(value: isize) -> Self {
        if value >= 0 {
            Self::pos(value as usize)
        } else {
            Self::neg((-value) as usize)
        }
    }
}

#[derive(Clone, Copy)]
pub(crate) struct LiteralDisplay<'a> {
    literal: Literal,
    assignment_stack: &'a AssignmentStack,
}

impl<'a> LiteralDisplay<'a> {
    pub fn new(literal: Literal, assignment_stack: &'a AssignmentStack) -> Self {
        Self {
            literal,
            assignment_stack,
        }
    }
}

impl std::fmt::Debug for LiteralDisplay<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let check_assignment = self.assignment_stack.check_assignment(self.literal);

        let string = match check_assignment {
            Some((_, dl)) => format!("{:?}@{dl}", self.literal),
            None => format!("{:?}", self.literal),
        };

        let colored_string = match check_assignment.map(|(v, _)| v) {
            Some(true) => string.green(),
            Some(false) => string.red(),
            None => string.normal(),
        };

        write!(f, "{}", colored_string)
    }
}
