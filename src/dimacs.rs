use anyhow::Context;
use bumpalo::{boxed::Box, Bump};

use crate::Literal;

#[derive(Debug)]
pub struct Dimacs<'bump> {
    pub var_count: usize,
    pub clauses: Vec<Box<'bump, [Literal]>>,
}

impl<'bump> Dimacs<'bump> {
    pub fn from_str(source: &str, bump: &'bump Bump) -> anyhow::Result<Self> {
        let mut lines = source.lines();

        let (var_count, clause_count) = read_header(&mut lines)?;
        let clauses = parse_clauses(clause_count, lines, bump)?;

        Ok(Self { var_count, clauses })
    }
}

fn parse_clauses<'bump>(
    clause_count: usize,
    lines: std::str::Lines<'_>,
    bump: &'bump Bump,
) -> Result<Vec<Box<'bump, [Literal]>>, anyhow::Error> {
    type BumpVec<'a, T> = bumpalo::collections::Vec<'a, T>;

    let mut clauses = Vec::with_capacity(clause_count);
    let mut clause = BumpVec::<Literal>::new_in(bump);

    let values = lines
        .filter(|line| !line.starts_with('c'))
        .flat_map(|line| line.split_whitespace());
    for value in values {
        let value: isize = value
            .parse()
            .with_context(|| format!("parsing '{value}' to integer"))?;
        if value == 0 {
            let mut new_clause = BumpVec::new_in(bump);
            std::mem::swap(&mut clause, &mut new_clause);
            clauses.push(new_clause.into_boxed_slice());
            if clauses.len() == clause_count {
                break;
            }
        } else {
            clause.push(value.into());
        }
    }

    Ok(clauses)
}

fn read_header(lines: &mut std::str::Lines<'_>) -> Result<(usize, usize), anyhow::Error> {
    for line in lines.by_ref() {
        if line.starts_with('c') {
            continue;
        } else if line.starts_with('p') {
            return parse_header(line).with_context(|| {
                format!("parsing {:?}", line.split_whitespace().collect::<Vec<_>>())
            });
        } else {
            anyhow::bail!("Invalid line: {line}");
        }
    }
    anyhow::bail!("missing header")
}

fn parse_header(line: &str) -> Result<(usize, usize), anyhow::Error> {
    let mut parts = line.split_whitespace();

    if parts.next() != Some("p") {
        anyhow::bail!("invalid line: {line}");
    }

    if parts.next() != Some("cnf") {
        anyhow::bail!("format other than 'cnf' not supported");
    }

    let var_count_str = parts
        .next()
        .ok_or_else(|| anyhow::anyhow!("unexpected end of line"))?;
    let var_count = var_count_str
        .parse()
        .with_context(|| format!("parsing '{var_count_str}' to integer"))?;

    let clause_count_str = parts
        .next()
        .ok_or_else(|| anyhow::anyhow!("unexpected end of line"))?;
    let clause_count = clause_count_str
        .parse()
        .with_context(|| format!("parsing '{clause_count_str}' to integer"))?;

    Ok((var_count, clause_count))
}
