use bumpalo::{boxed::Box, Bump};

use crate::{Literal, LiteralDisplay};

use super::AssignmentStack;

pub(crate) struct Clause<'bump>(Box<'bump, [Literal]>);

impl std::fmt::Debug for Clause<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_list().entries(self.0.iter()).finish()
    }
}

impl std::ops::Deref for Clause<'_> {
    type Target = [Literal];

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<'bump> Clause<'bump> {
    pub fn from_iter_in(iter: impl Iterator<Item = Literal>, bump: &'bump Bump) -> Self {
        let slice = bumpalo::collections::Vec::from_iter_in(iter, bump).into_boxed_slice();
        Self(slice)
    }
}

impl Clause<'_> {
    pub fn state(&self, assignment: &AssignmentStack) -> ClauseState {
        let mut unit_state = ClauseState::Conflict;
        for literal in self.0.iter().copied() {
            match assignment.check_assignment(literal) {
                Some((true, _)) => return ClauseState::Sat,
                None => {
                    unit_state = match unit_state {
                        ClauseState::Conflict => ClauseState::Unit(literal),
                        ClauseState::Unit(_) => return ClauseState::Unresolved,
                        _ => unreachable!(),
                    }
                }
                _ => (),
            }
        }

        unit_state
    }

    pub fn select_watched_literal_strict(
        &self,
        assignment: &AssignmentStack,
        exclude: Option<Literal>,
    ) -> Option<Literal> {
        for literal in self.iter().copied() {
            if exclude.map(Literal::var) == Some(literal.var()) {
                continue;
            }

            if let Some((false, _)) = assignment.check_assignment(literal) {
                continue;
            }

            return Some(literal);
        }

        None
    }

    pub fn select_watched_literal(
        &self,
        assignment: &AssignmentStack,
        exclude: Option<Literal>,
    ) -> Option<Literal> {
        for literal in self.iter().copied() {
            if exclude.map(Literal::var) == Some(literal.var()) {
                continue;
            }

            if let Some((false, _)) = assignment.check_assignment(literal) {
                continue;
            }

            return Some(literal);
        }

        // If the clause is implied or conflicting we pick a false literal with the highest
        // decision level.
        if self.state(assignment) != ClauseState::Unresolved {
            return self
                .iter()
                .copied()
                .filter(|&literal| Some(literal.var()) != exclude.map(Literal::var))
                .max_by(|&lit_a, &lit_b| {
                    let dl_a = assignment
                        .check_assignment(lit_a)
                        .map(|(_, dl)| dl)
                        .unwrap_or(0);
                    let dl_b = assignment
                        .check_assignment(lit_b)
                        .map(|(_, dl)| dl)
                        .unwrap_or(0);
                    dl_a.cmp(&dl_b)
                });
        }

        None
    }
}

impl<'bump> Clause<'bump> {
    pub fn display<'a>(
        &'a self,
        assignment_stack: &'a AssignmentStack,
    ) -> ClauseDisplay<'bump, 'a> {
        ClauseDisplay::new(self, assignment_stack)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(crate) enum ClauseState {
    Sat,
    Conflict,
    Unit(Literal),
    Unresolved,
}

#[derive(Debug, Clone, Copy)]
pub(crate) struct ClauseDisplay<'bump, 'a> {
    clause: &'a Clause<'bump>,
    assignment_stack: &'a AssignmentStack,
}

impl<'bump, 'a> ClauseDisplay<'bump, 'a> {
    pub fn new(clause: &'a Clause<'bump>, assignment_stack: &'a AssignmentStack) -> Self {
        Self {
            clause,
            assignment_stack,
        }
    }
}

impl std::fmt::Display for ClauseDisplay<'_, '_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_list()
            .entries(
                self.clause
                    .iter()
                    .map(|&literal| LiteralDisplay::new(literal, self.assignment_stack)),
            )
            .finish()
    }
}
