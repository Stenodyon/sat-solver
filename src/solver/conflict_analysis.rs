use log::*;

use crate::Literal;

use super::{Antecedent, Clause, ClauseIndex, Solver};

impl<'bump> Solver<'bump> {
    pub(super) fn analyze_conflict(
        &mut self,
        conflict_clause_index: ClauseIndex,
    ) -> ConflictAnalysisResult {
        debug!(
            target: "conflict_analysis",
            "Conflict on clause {} at decision level {}",
            &self.clauses[conflict_clause_index].display(&self.assignment_stack),
            self.decision_level
        );
        let conflict_clause = self.conflict_clause.get();
        conflict_clause.extend(self.clauses[conflict_clause_index].iter().copied());

        for &literal in conflict_clause.iter() {
            *self.score.entry(literal.var()).or_insert(0) += 1;
        }

        let asserted_literal = loop {
            let pivots = self.pivots.get();
            pivots.extend(conflict_clause.iter().copied().filter(|&literal| {
                self.assignment_stack.check_assignment(literal).unwrap().1 == self.decision_level
            }));

            trace!(target: "conflict_analysis", "pivots are {:?}", pivots);

            if pivots.len() == 1 {
                break pivots[0];
            }

            let pivot = self.assignment_stack.shallowest(pivots).unwrap();
            trace!(target: "conflict_analysis", "pivoting on {pivot:?}");
            match self.antecedent[&pivot] {
                Antecedent::Decision => unreachable!(),

                Antecedent::Implication(antecedent) => {
                    let antecedent = &self.clauses[antecedent];
                    for &literal in antecedent.iter() {
                        *self.score.entry(literal.var()).or_insert(0) += 1;
                    }
                    binary_resolve(conflict_clause, antecedent, pivot.var());
                    trace!(target: "conflict_analysis", "pivoting on {pivot:?} giving {:?}", conflict_clause);
                }

                Antecedent::OneLiteralClause => {
                    conflict_clause.swap_remove(
                        conflict_clause
                            .iter()
                            .position(|lit| lit.var() == pivot.var())
                            .unwrap(),
                    );
                }
            }
        };

        let backtrack_level = {
            // find the second highest decision level in the resulting clause, otherwise zero
            let max = conflict_clause
                .iter()
                .map(|&lit| self.assignment_stack.check_assignment(lit).unwrap().1)
                .max()
                .unwrap();
            conflict_clause
                .iter()
                .map(|&lit| self.assignment_stack.check_assignment(lit).unwrap().1)
                .filter(|&v| v < max)
                .max()
                .unwrap_or(0)
        };

        if conflict_clause.len() > 1 {
            let clause = Clause::from_iter_in(conflict_clause.drain(..), self.arena);
            debug!(
                target: "conflict_analysis",
                "learned {} asserting {asserted_literal:?}, backtracking to {backtrack_level}",
                clause.display(&self.assignment_stack)
            );
            let clause_index = self.clauses.len();
            self.clauses.push(clause);

            ConflictAnalysisResult::NewClause {
                backtrack_level,
                asserted_literal,
                clause_index,
            }
        } else {
            ConflictAnalysisResult::SingleLiteral(asserted_literal)
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(super) enum ConflictAnalysisResult {
    NewClause {
        backtrack_level: usize,
        asserted_literal: Literal,
        clause_index: ClauseIndex,
    },

    SingleLiteral(Literal),
}

impl ConflictAnalysisResult {
    pub fn backtrack_level(&self) -> usize {
        match *self {
            ConflictAnalysisResult::NewClause {
                backtrack_level, ..
            } => backtrack_level,

            ConflictAnalysisResult::SingleLiteral(_) => 0,
        }
    }

    pub fn asserted_literal(&self) -> Literal {
        match *self {
            ConflictAnalysisResult::NewClause {
                asserted_literal, ..
            } => asserted_literal,

            ConflictAnalysisResult::SingleLiteral(literal) => literal,
        }
    }

    pub fn asserted_literal_antecedent(&self) -> Antecedent {
        match *self {
            ConflictAnalysisResult::NewClause { clause_index, .. } => {
                Antecedent::Implication(clause_index)
            }

            ConflictAnalysisResult::SingleLiteral(_) => Antecedent::OneLiteralClause,
        }
    }
}

/// Performs binary resolution on `clause` and `other_clause` using `pivot`. After execution of the
/// function, `clause` is the resolvent clause.
///
/// The propositions `(A ∨ p) ∧ (B ∨ ¬p)` and `A ∨ B` are equisatisfiable (and equivalent?). `p` is
/// the pivot or resolution variable, `A` and `B` are the resolving clauses.
fn binary_resolve(clause: &mut Vec<Literal>, other_clause: &Clause, pivot: usize) {
    clause.swap_remove(clause.iter().position(|lit| lit.var() == pivot).unwrap());

    for literal in other_clause.iter().copied() {
        if literal.var() == pivot {
            continue;
        }

        if clause.contains(&literal) {
            continue;
        }

        clause.push(literal);
    }
}
