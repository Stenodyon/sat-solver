use log::*;

use super::{Antecedent, ClauseState, Solver};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub(super) enum BCPResult {
    Sat,
    Unresolved,
    Conflict(usize),
}

impl<'bump> Solver<'bump> {
    pub(super) fn bcp(&mut self) -> BCPResult {
        let mut conflict = None;

        loop {
            if let Some(clause_index) = conflict {
                self.propagation_queue.clear();
                return BCPResult::Conflict(clause_index);
            }

            let Some((literal, antecedent)) = self.propagation_queue.pop_front() else {
                break;
            };

            trace!(target: "bcp", "asserting and propagating {literal:?}");

            debug_assert_eq!(self.assignment_stack.check_assignment(literal), None);

            self.assignment_stack.assign(literal, self.decision_level);
            self.antecedent.insert(literal, antecedent);

            let clauses = self.watched_literals.entry(-literal).or_default().to_vec();

            for clause_index in clauses {
                let clause = &self.clauses[clause_index];
                let other_watched_literal = clause
                    .iter()
                    .copied()
                    .filter(|&lit| lit.var() != literal.var())
                    .find(|&lit| {
                        self.watched_literals
                            .entry(lit)
                            .or_default()
                            .contains(&clause_index)
                    })
                    .expect("All clauses should have two watched literals");

                trace!(target: "bcp", "in {}, other watched literal is {other_watched_literal:?}", clause.display(&self.assignment_stack));

                let other_assignment = self
                    .assignment_stack
                    .check_assignment(other_watched_literal)
                    .map(|(value, _)| value);

                if other_assignment == Some(true) {
                    trace!(target: "bcp", "Clause is already satisfied");
                    continue;
                }

                if let Some(new_watched_literal) = clause.select_watched_literal_strict(
                    &self.assignment_stack,
                    Some(other_watched_literal),
                ) {
                    self.watched_literals.move_watched_literal(
                        clause_index,
                        -literal,
                        new_watched_literal,
                    );

                    if self
                        .assignment_stack
                        .check_assignment(new_watched_literal)
                        .map(|(value, _)| value)
                        == Some(true)
                    {
                        trace!(target: "bcp", "Clause is already satisfied");
                        continue;
                    }

                    match clause.select_watched_literal_strict(
                        &self.assignment_stack,
                        Some(new_watched_literal),
                    ) {
                        Some(new_other_literal) if new_other_literal == other_watched_literal => (),

                        Some(new_other_literal) => self.watched_literals.move_watched_literal(
                            clause_index,
                            other_watched_literal,
                            new_other_literal,
                        ),

                        None => {
                            trace!(target: "bcp", "Clause is unit");

                            trace!(target: "literals", "asserting {new_watched_literal:?}@{}", self.decision_level);

                            debug_assert_eq!(
                                clause.state(&self.assignment_stack),
                                ClauseState::Unit(new_watched_literal)
                            );

                            if !self
                                .propagation_queue
                                .iter()
                                .any(|(lit, _)| lit.var() == new_watched_literal.var())
                            {
                                self.propagation_queue.push_back((
                                    new_watched_literal,
                                    Antecedent::Implication(clause_index),
                                ));
                            }
                        }
                    }
                } else if other_assignment.is_none() {
                    trace!(target: "bcp", "Clause is unit");

                    trace!(target: "literals", "asserting {other_watched_literal:?}@{}", self.decision_level);

                    debug_assert_eq!(
                        clause.state(&self.assignment_stack),
                        ClauseState::Unit(other_watched_literal)
                    );

                    if !self
                        .propagation_queue
                        .iter()
                        .any(|(lit, _)| lit.var() == other_watched_literal.var())
                    {
                        self.propagation_queue.push_back((
                            other_watched_literal,
                            Antecedent::Implication(clause_index),
                        ));
                    }
                } else if conflict.is_none() {
                    conflict = Some(clause_index);
                }
            }
        }

        let mut result_state = BCPResult::Sat;

        for state in self.clauses[..self.original_clause_count]
            .iter()
            .map(|clause| clause.state(&self.assignment_stack))
        {
            match state {
                ClauseState::Sat => (),
                ClauseState::Conflict | ClauseState::Unit(_) => unreachable!(),
                ClauseState::Unresolved => result_state = BCPResult::Unresolved,
            }
        }

        result_state
    }
}
