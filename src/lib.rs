mod dimacs;
mod literal;
mod solver;

pub use dimacs::*;
pub use literal::*;
pub use solver::*;

struct WorkVec<T>(Vec<T>);

impl<T> WorkVec<T> {
    pub fn new(inner: Vec<T>) -> Self {
        Self(inner)
    }

    pub fn get(&mut self) -> &mut Vec<T> {
        self.0.clear();
        &mut self.0
    }
}

impl<T: std::fmt::Debug> std::fmt::Debug for WorkVec<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}
