use anyhow::Context;
use bumpalo::Bump;
use clap::{Parser, Subcommand};
use sat::{Dimacs, SolveResult, Solver};

#[derive(Parser)]
#[command(version)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    Parse { filename: String },
    Solve { filename: String },
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init_timed();
    let cli = Cli::parse();

    match cli.command {
        Commands::Parse { filename } => {
            let file = std::fs::read_to_string(&filename)
                .with_context(|| format!("reading '{}'", &filename))?;
            let bump = Bump::new();
            let dimacs = Dimacs::from_str(&file, &bump)?;
            println!("{dimacs:?}");
        }

        Commands::Solve { filename } => {
            let file = std::fs::read_to_string(&filename)
                .with_context(|| format!("reading '{}'", &filename))?;
            let bump = Bump::new();
            let dimacs = Dimacs::from_str(&file, &bump)?;

            let mut solver = Solver::new(&bump);
            for clause in dimacs.clauses {
                solver.assert(clause.iter().copied());
            }

            match solver.solve() {
                SolveResult::Sat => {
                    println!("SATISFIABLE");
                    for literal in solver.model() {
                        print!("{literal} ");
                    }
                    println!();
                }

                SolveResult::Unsat => println!("UNSATISFIABLE"),
            }
        }
    }
    Ok(())
}
