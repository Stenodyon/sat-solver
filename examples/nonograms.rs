use anyhow::Context;
use bumpalo::Bump;
use clap::Parser;
use sat::{Literal, SolveResult, Solver};

#[derive(Parser)]
#[command(version)]
struct Cli {
    filename: String,
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let cli = Cli::parse();

    let file = std::fs::read_to_string(&cli.filename)
        .with_context(|| format!("reading '{}'", &cli.filename))?;

    let mut lines = file.lines();
    let header = lines.next().unwrap();
    let header = header
        .split_whitespace()
        .map(str::parse)
        .collect::<Result<Vec<usize>, _>>()?;
    let (width, height) = (header[0], header[1]);

    let var = |x: usize, y: usize| x + y * width;
    let mut current_var = width * height;
    let mut newvar = || {
        let value = current_var;
        current_var += 1;
        value
    };

    let rows = lines
        .by_ref()
        .take(height)
        .map(|line| {
            line.split_whitespace()
                .map(str::parse)
                .collect::<Result<Vec<usize>, _>>()
        })
        .collect::<Result<Vec<_>, _>>()?;

    let bump = Bump::new();
    let mut solver = Solver::new(&bump);

    let cols = lines
        .take(width)
        .map(|line| {
            line.split_whitespace()
                .map(str::parse)
                .collect::<Result<Vec<usize>, _>>()
        })
        .collect::<Result<Vec<_>, _>>()?;

    for (y, row) in rows.iter().enumerate() {
        let mut dnf: Vec<Vec<Literal>> = Vec::new();
        for possibility in fitting(row, width) {
            let mut map = vec![false; width];
            let mut current_offset = 0;
            for (index, offset) in possibility.iter().copied().enumerate() {
                current_offset += offset;
                for i in 0..row[index] {
                    map[i + current_offset] = true;
                }
                current_offset += row[index];

                if index < possibility.len() + 1 && current_offset < map.len() - 1 {
                    current_offset += 1;
                }
            }

            dnf.push(
                (0..map.len())
                    .map(|x| var(x, y))
                    .zip(map)
                    .map(|(var, present)| {
                        if present {
                            Literal::pos(var)
                        } else {
                            Literal::neg(var)
                        }
                    })
                    .collect(),
            );
        }
        assert_dnf(&dnf, &mut solver, &mut newvar);
    }

    for (x, col) in cols.iter().enumerate() {
        let mut dnf: Vec<Vec<Literal>> = Vec::new();
        for possibility in fitting(col, height) {
            let mut map = vec![false; height];
            let mut current_offset = 0;
            for (index, offset) in possibility.iter().copied().enumerate() {
                current_offset += offset;
                for i in 0..col[index] {
                    map[i + current_offset] = true;
                }
                current_offset += col[index];

                if index < possibility.len() + 1 && current_offset < map.len() - 1 {
                    current_offset += 1;
                }
            }

            dnf.push(
                (0..map.len())
                    .map(|y| var(x, y))
                    .zip(map)
                    .map(|(var, present)| {
                        if present {
                            Literal::pos(var)
                        } else {
                            Literal::neg(var)
                        }
                    })
                    .collect(),
            );
        }
        assert_dnf(&dnf, &mut solver, &mut newvar);
    }

    println!(
        "{} vars and {} clauses",
        solver.var_count(),
        solver.clause_count()
    );

    match solver.solve() {
        SolveResult::Sat => {
            let model = solver.model();

            for y in 0..height {
                for x in 0..width {
                    if model.contains(&Literal::pos(var(x, y))) {
                        print!("#");
                    } else {
                        print!(" ");
                    }
                }
                println!();
            }
        }

        SolveResult::Unsat => println!("UNSATISFIABLE"),
    }

    Ok(())
}

fn fitting(values: &[usize], space: usize) -> Vec<Vec<usize>> {
    if values.len() == 1 {
        if values[0] > space {
            return Vec::new();
        }
        return (0..space + 1 - values[0]).map(|v| Vec::from([v])).collect();
    }

    if values[0] + 1 > space {
        return Vec::new();
    }

    let value = values[0];
    let mut fit = Vec::new();
    for offset in 0..space - value - 1 {
        for solution in fitting(&values[1..], space - offset - value - 1) {
            let mut vec = vec![offset; 1];
            vec.extend(solution);
            fit.push(vec);
        }
    }
    fit
}

fn assert_dnf(dnf: &[Vec<Literal>], solver: &mut Solver<'_>, mut newvar: impl FnMut() -> usize) {
    let mut standins = Vec::new();
    for row in dnf {
        use std::iter::once;

        let standin = newvar();
        standins.push(standin);
        for literal in row.iter().copied() {
            solver.assert([Literal::neg(standin), literal].into_iter());
        }

        solver.assert(
            row.iter()
                .map(|&lit| -lit)
                .chain(once(Literal::pos(standin))),
        );
    }
    solver.assert(standins.into_iter().map(Literal::pos));
}
