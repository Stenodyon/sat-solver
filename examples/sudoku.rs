use anyhow::Context;
use bumpalo::Bump;
use clap::Parser;
use colored::Colorize;
use sat::{Literal, SolveResult, Solver};

const NUMBERS: [&str; 10] = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];

#[derive(Parser)]
#[command(version)]
struct Cli {
    filename: String,
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init();
    let cli = Cli::parse();

    let input_board = load_board(cli.filename)?;
    print_input_board(&input_board);

    let bump = Bump::new();
    let mut solver = Solver::new(&bump);

    generate_clauses(&mut solver, &input_board);

    println!("---");
    println!(
        "Var count: {}, clause count: {}",
        solver.var_count(),
        solver.clause_count()
    );
    println!("---");

    match solver.solve() {
        SolveResult::Sat => {
            let result = solution_from_model(&solver);
            print_reuslt(result, input_board);
        }

        SolveResult::Unsat => println!("UNSATISFIABLE"),
    }

    Ok(())
}

fn generate_clauses(solver: &mut Solver<'_>, input_board: &Board<Option<usize>>) {
    // exactly one value per cell
    for col in 1..=9 {
        for row in 1..=9 {
            let vars = (1..=9)
                .map(|value| var(col, row, value))
                .map(Literal::pos)
                .collect::<Vec<_>>();
            solver.assert_exactly_one(&vars);
        }
    }

    // exactly one of each value per row
    for row in 1..=9 {
        for value in 1..=9 {
            let vars = (1..=9)
                .map(|col| var(col, row, value))
                .map(Literal::pos)
                .collect::<Vec<_>>();
            solver.assert_exactly_one(&vars);
        }
    }

    // exactly one of each value per column
    for col in 1..=9 {
        for value in 1..=9 {
            let vars = (1..=9)
                .map(|row| var(col, row, value))
                .map(Literal::pos)
                .collect::<Vec<_>>();
            solver.assert_exactly_one(&vars);
        }
    }

    // exactly one of each value per square
    for square_x in 0..3 {
        for square_y in 0..3 {
            let positions = (square_y * 3..(square_y + 1) * 3)
                .flat_map(|row| {
                    (square_x * 3..(square_x + 1) * 3).map(move |col| (col + 1, row + 1))
                })
                .collect::<Vec<_>>();

            for value in 1..=9 {
                let vars = positions
                    .iter()
                    .copied()
                    .map(|(col, row)| var(col, row, value))
                    .map(Literal::pos)
                    .collect::<Vec<_>>();
                solver.assert_exactly_one(&vars);
            }
        }
    }

    // assert the input values to be true
    for (col, row, value) in input_board.iter_sparse() {
        solver.assert([Literal::pos(var(col + 1, row + 1, value))].into_iter());
    }
}

fn var(col: usize, row: usize, value: usize) -> usize {
    col - 1 + (row - 1 + (value - 1) * 9) * 9
}

fn from_var(value: usize) -> (usize, usize, usize) {
    let col = value.rem_euclid(9);
    let rest = value.div_euclid(9);
    let row = rest.rem_euclid(9);
    let value = rest.div_euclid(9);
    (col, row, value)
}

struct Board<T> {
    data: Vec<T>,
}

impl<T: Copy> Board<T> {
    pub fn new(fill: T) -> Self {
        Self {
            data: vec![fill; 9 * 9],
        }
    }

    pub fn set(&mut self, col: usize, row: usize, value: T) {
        self.data[col + row * 9] = value;
    }

    pub fn get(&self, col: usize, row: usize) -> T {
        self.data[col + row * 9]
    }

    pub fn iter(&self) -> impl Iterator<Item = (usize, usize, T)> + '_ {
        self.data
            .iter()
            .copied()
            .enumerate()
            .map(|(index, value)| (index.rem_euclid(9), index.div_euclid(9), value))
    }
}

impl<T: Copy> Board<Option<T>> {
    pub fn iter_sparse(&self) -> impl Iterator<Item = (usize, usize, T)> + '_ {
        self.data.iter().enumerate().filter_map(|(index, value)| {
            value.map(|v| (index.rem_euclid(9), index.div_euclid(9), v))
        })
    }
}

fn load_board(filename: impl AsRef<std::path::Path>) -> anyhow::Result<Board<Option<usize>>> {
    let file = std::fs::read_to_string(filename.as_ref())
        .with_context(|| format!("reading '{:?}'", filename.as_ref()))?;

    let mut input_board = Board::new(None::<usize>);

    for (row, line) in file.lines().enumerate() {
        let values: Vec<_> = line.split_whitespace().collect();

        if values.len() != 9 {
            eprintln!("invalid line {}", row + 1);
            std::process::exit(1);
        }

        for (col, value) in values.iter().enumerate() {
            if *value == "." {
                continue;
            }
            let value = value.parse().unwrap();
            input_board.set(col, row, Some(value));
        }
    }

    Ok(input_board)
}

fn print_input_board(input_board: &Board<Option<usize>>) {
    for row in 1..=9 {
        for col in 1..=9 {
            if let Some(value) = input_board.get(col - 1, row - 1) {
                print!("{value} ");
            } else {
                print!(". ");
            }

            if col % 3 == 0 && col < 9 {
                print!("| ");
            }
        }
        println!();
        if row % 3 == 0 && row < 9 {
            println!("------+-------+------")
        }
    }
}

fn solution_from_model(solver: &Solver<'_>) -> Board<usize> {
    let mut result = Board::new(0usize);

    for literal in solver.model() {
        if literal.is_pos() {
            let (col, row, value) = from_var(literal.var());
            if result.get(col, row) != 0 {
                eprintln!("ERROR: two values assigned at {col},{row}");
            }
            result.set(col, row, value + 1);
        }
    }

    if let Some((col, row, _)) = result.iter().find(|(_, _, v)| *v == 0) {
        eprintln!("ERROR: missing value at ({}, {})", col + 1, row + 1);
    }

    result
}

fn print_reuslt(result: Board<usize>, input_board: Board<Option<usize>>) {
    for (col, row, result_value) in result.iter() {
        let c = if let Some(value) = input_board.get(col, row) {
            if value == result_value {
                NUMBERS[result_value].cyan()
            } else {
                NUMBERS[result_value].red()
            }
        } else if result_value == 0 {
            NUMBERS[result_value].red()
        } else {
            NUMBERS[result_value].normal()
        };
        print!("{} ", c);

        if col % 3 == 2 && col < 8 {
            print!("{}", "| ".dimmed());
        }
        if col == 8 {
            println!();
            if row % 3 == 2 && row < 8 {
                println!("{}", "------+-------+------".dimmed());
            }
        }
    }
}
